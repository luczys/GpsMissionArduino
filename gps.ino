#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <GpsMissionBtCodec.h>

#define POS2FLOAT(pos) ((float) pos / 1000000.0)

long prevPosLat, prevPosLon; // previous position
long posLat, posLon; // current position
long tarLat, tarLon; // target position
bool gettingPos;
bool aimingTarget;
float course;

SoftwareSerial gpsSerial(8, 9); // create gps sensor connection
TinyGPS gps; // create gps object
GpsMissionBtCodec btCodec;

void setup() {
  prevPosLat = 0;
  prevPosLon = 0;
  posLat = 0;
  posLon = 0;
  tarLat = 0;
  tarLon = 0;
  gettingPos = false;
  aimingTarget = false;
  course = 0;

  Serial.begin(9600); // connect serial
  gpsSerial.begin(9600); // connect gps sensor
  Serial1.begin(9600); // bt serial
}

void loop() {
  while (gpsSerial.available()) { // check for gps data
    char val = gpsSerial.read();
    //Serial.print("GPS data: "); Serial.println(val, HEX); // gps data
    Serial.print(val);
    if (gps.encode(val)) { // encode gps data
      prevPosLat = posLat;
      prevPosLon = posLon;
      gps.get_position(&posLat, &posLon); // get latitude and longitude
      course = gps.f_course();
      // display position
      Serial.print("Position: ");
      Serial.print("lat: "); Serial.print(posLat); Serial.print(" "); // print latitude
      Serial.print("lon: "); Serial.println(posLon); // print longitude
      btCodec.encode(GpsMissionBtCodec::positionHeader, posLat, posLon);
      Serial1.write(btCodec.get_buffer(), btCodec.get_len());
      gettingPos = true;
      adjustCourse();
    }
  }
  while (Serial1.available())
  {
    char val = Serial1.read();
    Serial.print("BT data: "); Serial.println(val, HEX); // Bluetooth data
    if (btCodec.decode(val))
    {
      if (btCodec.get_header() == GpsMissionBtCodec::targetHeader)
      {
        btCodec.get_params(&tarLat, &tarLon);
        Serial.print("Target: ");
        Serial.print("lat: "); Serial.print(tarLat); Serial.print(" "); // print latitude
        Serial.print("lon: "); Serial.println(tarLon); // print longitude
        aimingTarget = true;
        adjustCourse();
//#define ECHOTEST
#ifdef ECHOTEST
        btCodec.encode(GpsMissionBtCodec::positionHeader, tarLat, tarLon);
        Serial1.write(btCodec.get_buffer(), btCodec.get_len());
        btCodec.encode(GpsMissionBtCodec::relativeCourseHeader, 90.0 * 100);
        Serial1.write(btCodec.get_buffer(), btCodec.get_len());
#endif
      }
    }
  }
}

void adjustCourse()
{
  if (gettingPos && aimingTarget)
  {
    float f_posLat = POS2FLOAT(posLat);
    float f_posLon = POS2FLOAT(posLon);
    float f_tarLat = POS2FLOAT(tarLat);
    float f_tarLon = POS2FLOAT(tarLat);
    float distance = gps.distance_between(f_posLat, f_posLon, f_tarLat, f_tarLon);
    Serial.print("Distance to target: "); Serial.println(distance);

    if (distance > 5)
    {
      float relativeCourse = course - gps.course_to(f_posLat, f_posLon, f_tarLat, f_tarLon);
      if (relativeCourse > 180.0)
      {
        relativeCourse -= 360.0;
      }
      else if (relativeCourse < -180.0)
      {
        relativeCourse += 360.0;
      }
      adjustMovement(relativeCourse);
      btCodec.encode(GpsMissionBtCodec::relativeCourseHeader, relativeCourse * 100);
      Serial1.write(btCodec.get_buffer(), btCodec.get_len());
    }
    else
    {
      fullStop();
      aimingTarget = false;
    }
  }
}

void adjustMovement(float relativeCourse)
{
  Serial.print("Movement: "); Serial.println(relativeCourse);
}

void fullStop()
{
  Serial.println("Full stop.");
}
