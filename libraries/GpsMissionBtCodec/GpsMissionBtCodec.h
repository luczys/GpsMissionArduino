
#ifndef GpsMissionBtCodec_h
#define GpsMissionBtCodec_h

#include "Arduino.h"

class GpsMissionBtCodec
{
public:
  enum
  {
    MAX_MESSAGE_SIZE = 1 + 2 * 4,
    MAX_PACKED_MESSAGE_SIZE = (1 + 3 * 4) * 2 + 2,
  };
  
  static const byte targetHeader = 0x00;
  static const byte positionHeader = 0x01;
  static const byte relativeCourseHeader = 0x02;
  
  GpsMissionBtCodec();

  bool decode(byte c);                             
  byte get_header();
  void get_params(long *par1, long *par2);
  
  void encode(byte header, long par); 
  void encode(byte header, long par1, long par2);   
  
  byte* get_buffer(); 
  byte get_len(); 

private:
  const byte startFlag = 0xFE;
  const byte escapeFlag = 0xFD;
  const byte escapeMod = 0xDF;
  const byte unescapeMod = 0x20;
  
  byte decodeBuff[MAX_MESSAGE_SIZE];
  byte decodeBuffOff;
  bool inDecodeBuff;
  bool decodeEscapeFlag;
  
  byte encodeBuff[MAX_PACKED_MESSAGE_SIZE];
  byte encodeBuffOff;
  
  void encodeStart(byte header);
  void encodeLong(long val);
  void encodeByte(byte val);
  void encodeStop();
};

#endif
