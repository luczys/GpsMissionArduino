#include "GpsMissionBtCodec.h"

#define DEBUG_LOG(x) Serial.println(x)

GpsMissionBtCodec::GpsMissionBtCodec()
    : decodeBuffOff(0),
      inDecodeBuff(false), decodeEscapeFlag(false)
{
}

bool GpsMissionBtCodec::decode(byte c)
{
  DEBUG_LOG("Decode enter");
  if (!inDecodeBuff)
  {
	DEBUG_LOG(" Decode not in buff");
    if (c == startFlag)
    {
	  DEBUG_LOG("  Decode not in buff start flag");
      inDecodeBuff = true;
      decodeBuffOff = 0;
    }
  }
  else
  {
	DEBUG_LOG(" Decode in buff");
    if (c == startFlag)
    {
	  DEBUG_LOG("  Decode in buff start flag");
	  if ( decodeBuffOff > 0)
	  {
		DEBUG_LOG("   Decode in buff start flag and buff > 0");
		inDecodeBuff = false;
		decodeEscapeFlag = false;
		return true;
	  }
    }
    else
    {
	  DEBUG_LOG("  Decode in buff");
      if (c != escapeFlag)
      {
	    DEBUG_LOG("   Decode in buff no escape");
	    decodeBuff[decodeBuffOff] = c;
        if (decodeEscapeFlag)
        {
		  DEBUG_LOG("    Decode in buff no escape prev escape");
	      decodeBuff[decodeBuffOff] |= unescapeMod;
        }
        decodeBuffOff++;
        decodeEscapeFlag = false;
        if (decodeBuffOff > MAX_MESSAGE_SIZE)
        {
		  DEBUG_LOG("    Decode in buff no escape over max");
          decodeBuffOff = 0;
          inDecodeBuff = false;
        }
      }
      else
      {
	    DEBUG_LOG("   Decode in buff escape");
        decodeEscapeFlag = true;
      }
    }
  }
  return false;
}

byte GpsMissionBtCodec::get_header()
{
  return decodeBuff[0];
}

void GpsMissionBtCodec::get_params(long *par1, long *par2)
{
  *par1 = (long)decodeBuff[1] | ((long)decodeBuff[2] << 8) | ((long)decodeBuff[3] << 16) | ((long)decodeBuff[4] << 24);
  *par2 = (long)decodeBuff[5] | ((long)decodeBuff[6] << 8) | ((long)decodeBuff[7] << 16) | ((long)decodeBuff[8] << 24);
}

void GpsMissionBtCodec::encode(byte header, long par)
{
  encodeStart(header);
  encodeLong(par);  
  encodeStop();	
}
void GpsMissionBtCodec::encode(byte header, long par1, long par2)
{
  encodeStart(header);
  encodeLong(par1);  
  encodeLong(par2);  
  encodeStop();	
}


byte* GpsMissionBtCodec::get_buffer()
{
  return encodeBuff;  
}

byte GpsMissionBtCodec::get_len()
{
  return encodeBuffOff;
}

void GpsMissionBtCodec::encodeStart(byte header)
{
  encodeBuff[0] = startFlag;
  encodeBuffOff = 1;	
  encodeByte(header);
}

void GpsMissionBtCodec::encodeLong(long val)
{
  encodeByte(val);
  encodeByte(val >> 8);
  encodeByte(val >> 16);
  encodeByte(val >> 24);	
}

void GpsMissionBtCodec::encodeByte(byte val)
{
  if (val != startFlag && val != escapeFlag)
  {
	encodeBuff[encodeBuffOff++] = val;
  }
  else
  {
	encodeBuff[encodeBuffOff++] = escapeFlag;
	encodeBuff[encodeBuffOff++] = val & escapeMod;
  }    
}

void GpsMissionBtCodec::encodeStop()
{
  encodeBuff[encodeBuffOff++] = startFlag;
}


